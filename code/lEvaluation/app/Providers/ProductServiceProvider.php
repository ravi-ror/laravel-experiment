<?php

namespace App\Providers;

use App\Events\SearchEvent;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobProcessing;
use Illuminate\Queue\Queue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;

class ProductServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        /*Queue::before(function (JobProcessing $event) {
            Log::info('Event Job ');
        });

        Queue::after(function (JobProcessed $event) {
            Log::info('[QUEUE COMPLETE]', $event->job->getName());
            event(new SearchEvent(json_decode( $event->job->getRawBody() )));
        });*/
    }
}
