<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessSearch;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $searchParams = $request->only('product_name', 'price', 'rating');
        $accessToken = $request->header('Authorization');
        $job = (new ProcessSearch($searchParams, $accessToken))->onQueue('search');
        $this->dispatch($job);
        return response()->json([
            'status' => 'success',
            'msg' => 'In progress.'
        ], 200);
    }
}
