<?php


namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;

class RegisterController extends BaseController
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        try {
            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
            $user = User::create($input);
            $success['token'] = $user->createToken('lEvaluation')->accessToken;
            $success['name'] = $user->name;
        }catch (\Illuminate\Database\QueryException $ex) {
            return $this->sendError('Duplication', ['error'=> 'User entry or Email address is already exist. If error persists, please contact support team - support@company.com']);
        }
        return $this->sendResponse($success, 'User register successfully.');
    }

    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('lEvaluation')-> accessToken;
            $success['name'] =  $user->name;

            return $this->sendResponse($success, 'User login successfully.');
        }
        else{
            return $this->sendError('Unauthorised.', ['error'=>'Invalid email or password. Please enter valid credentials.']);
        }
    }
	
	/**
	 * Validate Request Api
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function guard(Request $request)
    {
        if (Auth::guard('api')->check() == true) {
			return response()->json(['success' => true]);
		}
		return response()->json(['success' => false]);
    }
}
