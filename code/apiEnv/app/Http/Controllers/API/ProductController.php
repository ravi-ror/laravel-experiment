<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Product;
use App\Http\Resources\Product as ProductResource;

class ProductController extends BaseController
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
		$searchTerm = ($request->product_name)?$request->product_name:'';
		$maxPrice = ($request->price != '')?$request->price:20000;
		$maxRate = ($request->rating != '')?$request->rating:5;
		
        $products = Product::where('name', 'LIKE','%'.$searchTerm."%")
				->orWhere('detail','LIKE','%'.$searchTerm.'%')
				->Where('amount', '<=', $maxPrice)
				->Where('rating', '<=', $maxRate)
				->get();

        return $this->sendResponse($products, 'Products retrieved successfully.');
    }
}