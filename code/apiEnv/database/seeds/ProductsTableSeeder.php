<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name' => 'CASIO',
            'detail' => 'Edifice Men Black Dial Chronograph Watch EFR-526L-1AVUDF - EX096',
            'amount' => '6781',
            'rating' => '4'
        ],
        [
            'name' => 'Daniel Klein',
            'detail' => 'Men Blue Dual Time Analogue Watch DK11125-5',
            'amount' => '2901',
            'rating' => '5'
        ],
        [
            'name' => 'Fossil',
            'detail' => 'Women Rose Gold-Toned Analogue Watch BQ1561_OR',
            'amount' => '5012',
            'rating' => '3'
        ],
        [
            'name' => 'Titan',
            'detail' => 'Women Pink Analogue Watch 95118WM01',
            'amount' => '15231',
            'rating' => '5'
        ],[
            'name' => 'GUESS',
            'detail' => 'Women White Analogue Watch',
            'amount' => '7819',
            'rating' => '2'
        ],[
            'name' => 'DIESEL',
            'detail' => 'Men Gunmetal-Toned Chronograph Watch DZ4463I',
            'amount' => '16992',
            'rating' => '1'
        ]);
    }
}
