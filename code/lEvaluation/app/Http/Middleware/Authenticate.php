<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Str;

class Authenticate extends Middleware
{
    public function handle($request, Closure $next, ...$guards)
    {
        $url = $request->url();
        if(Str::contains($url, 'user') || Str::contains($url, 'search')){
            $accessToken = $request->header('Authorization');
            $accessToken = Str::replaceFirst("Bearer ","", $accessToken);
            if (!$accessToken) {
                return response()->json(['error'=>'Unauthorized']);
            }
        }
        return $next($request);
    }
}
