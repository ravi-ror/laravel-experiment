<?php

namespace App\Providers;

use App\Events\SearchEvent;
use Illuminate\Support\Facades\Queue;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Queue\Events\JobProcessing;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /*Queue::before(function (JobProcessing $event) {
            Log::info('Event Job ');
        });*/
        /*Queue::after(function (JobProcessed $event) {
            //event(new SearchEvent(json_decode( $event->job->getRawBody() )));
            Log::info('Event Job end');
        });*/
    }
}
