<?php

namespace App\Jobs;

use App\Events\SearchEvent;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Http\Controllers\SearchController;
use Illuminate\Support\Facades\Log;

class ProcessSearch implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $searchParams;
    public $accessToken;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($searchParams, $accessToken)
    {
        $this->searchParams = $searchParams;
        $this->accessToken = $accessToken;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $client = new Client();
        $request = $client->post('http://apienv.test/api/products',
            ['form_params' => $this->searchParams],
            ['headers' => [
                'Authorization' => $this->accessToken,
                'Accept' => 'application/json'
            ]]);
        $response = $request->getBody()->getContents();
        event(new SearchEvent($response));
    }
}
