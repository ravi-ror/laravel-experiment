<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Str;

class AuthController extends Controller
{

    /**
     * Registeration Request Api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $v = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:3',
            'c_password'  => 'required|min:3|same:password',
        ]);
        if ($v->fails())
        {
            return response()->json([
                'status' => 'registration_validation_error',
                'errors' => $v->errors()
            ], 422);
        }
        $credentials = $request->all();
        try {
            $client = new Client();
            $request = $client->post('http://apienv.test/api/register', ['form_params' => $credentials], ['headers' => ['Accept' => 'application/json']]);
            $resp = $request->getBody();
            $resObj = json_decode($resp);
            return response()->json(['status' => 'success'], 200)->header('Authorization', $resObj->data->token);
        }
        catch (BadResponseException $exception){
            $response = $exception->getResponse();
            return $responseBodyAsString = $response->getBody()->getContents();
        }
    }

    /**
     * Login Api Call
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $v = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:3'
        ]);
        if ($v->fails())
        {
            return response()->json([
                'status' => 'registration_validation_error',
                'errors' => $v->errors()
            ], 422);
        }
        $credentials = $request->only('email', 'password');
        try {
            $client = new Client();
            $request = $client->post('http://apienv.test/api/login', ['form_params' => $credentials], ['headers' => ['Accept' => 'application/json']]);
            $resp = $request->getBody();
            $resObj = json_decode($resp);
            return response()->json(['status' => 'success'], 200)->header('Authorization', $resObj->data->token);
        }
        catch (BadResponseException $exception){
            $response = $exception->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            if($responseBodyAsString->success == false){
                return response()->json([
                    'status' => 'registration_validation_error',
                    'errors' => $responseBodyAsString->error
                ], 422);
            }
        }
    }

    /**
     * Logout Api Call
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        return response()->json([
            'status' => 'success',
            'msg' => 'Logged out Successfully.'
        ], 200);
    }

    /**
     * Validate Request Api
     *
     * @return \Illuminate\Http\Response
     */
    public function user(Request $request)
    {
        $accessToken = $request->header('Authorization');
        $client = new Client();
        $response = $client->request('POST', 'http://apienv.test/api/user/', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => $accessToken,
            ],
        ]);
        $resp = $response->getBody();
        $resObj = json_decode($resp);
        if($resObj->success === false){
            return response()->json(['error' => 'refresh_token_error'], 401);
        }
        return response()->json(['success' => 'Valid User!'], 200);
    }
    public function refresh()
    {
        echo 'valid';
    }
}
